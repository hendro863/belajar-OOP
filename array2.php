<?php

$array = [
    'name'=> [
        'hendro',
        'rahmat',
        'no_hp' => [
            '1234567',
            '098765'
        ]
    ],
    'alamat'=> [
        'jl tojoyo',
        'jl hayam wuruk',
        'no_hp' => [
            '1234567',
            '098765'
        ]
    ],
    'password'=>[
        'alhamdulillah',
        'gakperlumbulet'
    ]
];

function run(array $items)
{
    $i=0;
    foreach ($items as $item => $value) {
        foreach ($value as $v) {
            if (is_array($v)) {
                foreach ($v as $k) {
                    echo $k.'<br>';
                }
            }
        }
    }
}

run($array);