<?php

/**
*
*/
class Robot
{
	protected $canFly;
	protected $legCount;

	public function __construct($legCount,$canFly)
	{
		$this->canFly = $canFly;
		$this->legCount = $legCount;
	}

	public function canFly()
	{
		return $this->canFly;
	}

	public function getLegCount()
	{
		return "Hay ".$this->legCount;
	}
}