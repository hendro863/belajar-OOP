<?php
/**
*
*/
class Person
{

    public $firstName;
    private $secret ;
    private $request = array();

    public function __set($name, $value)
    {
        echo '<strong>'.$name.'</strong> Setting to <em>'.$value.'</em><br />';
        $this->request[$name] = $value;
    }

}

$test = new Person;

echo "<pre>".print_r($test,true).'</pre>';
$test->firstName = "Hendro";
echo '<pre>'.print_r($test,true).'</pre>';
$test->nickName = "Rahmat";
echo "<pre>".print_r($test,true)."</pre>";
$test->secret ='My Secret' ;
echo "<pre>".print_r($test,true)."</pre>";