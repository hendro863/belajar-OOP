<?php
/**
*
*/
class Customer
{

    private $cash = 20000;

    public function getCash()
    {
        return $this->cash;
    }

    public function setCash($value)
    {
       if (is_int($value)) {
        $this->cash = $value;
       }
    }
}

$test = new Customer;
// $test->cash = 50000;
// echo $test->cash;
echo $test->getCash()."<br>";
$test->setCash(9000);
echo $test->getCash();