<?php
/**
*
*/
/* magic method call digunakan untuk memanggil method pada class yg tidak
terdefinisikan pada class */
class Person
{

    public function __call($name, $arguments)
    {
        echo "--Calling object method <b>{$name}</b><br>";
        echo "--arguments of method : <pre>".var_export($arguments,true)."</pre>";
        echo "--funct_get_args : <pre>".var_export(func_get_args(),true)."</pre>";
        return '<emp>'.implode('|', $arguments).'</em>';
    }
}

$test =  new Person;
echo $test->coba('foo','bar','bas');