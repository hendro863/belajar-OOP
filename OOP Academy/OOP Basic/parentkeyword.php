<?php
/**
*
*/
class Animal
{

    const NOISE = 'Moo';

    public static function makeNoise()
    {
        return '<strong>'.self::NOISE.'</strong><br />';
    }
}

class Pet extends Animal
{
    const NOISE = 'Foo';

    public static function makeNoise()
    {
        return '<em>'.parent::NOISE.'</em><br />';
    }

    public function test()
    {
        echo __CLASS__;
    }
}

class Cat extends Pet
{
    const NOISE = 'Bar';

    public static function makeNoise()
    {
        return '<ins>'.parent::NOISE.'</ins>';
    }

    public static function instance()
    {
        return new parent();
    }


}

echo 'Animal : '.Animal::makeNoise();
echo 'Pet : '.Pet::makeNoise();
echo 'Cat : '.Cat::makeNoise();

$test = Cat::instance();
echo gettype($test);
$test->test();