<?php
/**
*
*/
class Person
{
    public $firstName;
    public $lastName;

    function __construct($fName,$lName)
    {
        $this->firstName = $fName;
        $this->lastName = $lName;

        echo $this->fullName()."<br>";
    }

    public function __destruct()
    {
        echo "Bye".$this->fullName()."<br>";
    }

    public function fullName()
    {
        return $this->firstName.' '.$this->lastName;
    }
}

$test = new Person("Hendro", "Rahmat");
$jhon = new Person("Senda", "Hendy");
$rambo = new Person("Ilham", "Rendy");
unset($test);
