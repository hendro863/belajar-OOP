<?php
/**
*
*/
class Animal
{
    const NOISE = "Moo!";

    public static function makeNoise()
    {
        return '<strong>'.self::NOISE.'</strong><br />';
    }

}

class Pet extends Animal
{
    const NOISE = 'Kwek';

    public static function makeNoise()
    {
        return '<ins>'.self::NOISE.'</ins><br />';
    }
}

class Cat extends Pet
{
    const NOISE = 'Meong!';

    // public static function makeNoise()
    // {
    //     return '<em>'.self::NOISE.'</em><br />';
    // }

    public static function instance()
    {
        return new self();
    }

    public function test()
    {
        echo __CLASS__;
    }
}

echo "Animal : ".Animal::makeNoise();
echo "Pet : ".Pet::makeNoise();
echo "Cat : ".Cat::makeNoise();

$tom = Cat::instance();
echo gettype($tom).'<br>';
$tom->test();