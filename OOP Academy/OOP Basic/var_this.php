<?php
/**
*
*/
class Person
{
    var $first_name;
    var $last_name;

    function full_name()
    {
        return $this->first_name."{$this->last_name}";
    }

    function this_object()
    {
        echo gettype($this)."<br>";
    }

    function say_hello()
    {
        echo 'Hai '."{$this->full_name()}<br>";
    }
}

$orang = new Person;
$hewan = new Person;

$orang->first_name = 'Hendro';
$orang->last_name  = ' Rahmat';
$hewan->first_name = "Kebo";
$hewan->last_name  = " Giras";

echo $orang->full_name()."<br>";
echo $hewan->full_name()."<br>";
$orang->this_object();
$hewan->say_hello();