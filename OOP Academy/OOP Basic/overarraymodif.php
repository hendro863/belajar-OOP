<?php
/**
*
*/
class Person
{
    public $firstName;
    private $secret;
    private $request = array();

    public function __set($name, $value)
    {
        echo "---<b>{$name}</b> Setting to <i>".var_export($value, true).'</i><br>';
        return $this->request[$name] = $value;
    }

    public function __get($name)
    {
        echo "--Getting <u> {$name} </u><br />";
        if (array_key_exists($name, $this->request)) {
            return $this->request[$name];
        }
    }

    public function __isset($name)
    {
        echo "is {$name} already set ? ";
        return isset($this->request[$name])."</br>";
    }

    public function __unset($name)
    {
        echo '<pre>'.print_r($this,true).'</pre>';
        unset($this->request[$name]) ;
    }

}

$test = new Person();
$test->firstName = "Hendro";
$test->friends = array('Senda', 'Rahmat','Saputra','Rendy');
$friends = $test->friends;
array_push($friends, 'John');
$test->friends = $friends;
echo '<pre>'.print_r($test,true).'</pre>';
