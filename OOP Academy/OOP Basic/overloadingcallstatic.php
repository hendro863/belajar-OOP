<?php
/**
*
*/
class Person
{

    public static function __callStatic($name, $arguments)
    {
        echo "--Calling static method <b>{$name}</b><br>";
        echo "--arguments of method : <pre>".var_export($arguments,true)."</pre>";
        echo "--funct_get_args : <pre>".var_export(func_get_args(),true)."</pre>";
        return '<strong><emp>'.implode('|', $arguments).'</em></strong>';
    }

    private static function coba()
    {
        echo "Aksesk Private!";
    }

}

echo Person::coba('foo','bar','baz') ;
