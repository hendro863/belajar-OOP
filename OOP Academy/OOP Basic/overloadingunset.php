<?php
/**
*
*/
class Person
{
    public $firstName;
    private $secret;
    private $request = array();

    public function __set($name, $value)
    {
        return $this->request[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->request)) {
            return $this->request[$name];
        }
    }

    public function __isset($name)
    {
        return isset($this->request[$name])."</br>";
    }

    public function __unset($name)
    {
        unset($this->request[$name]) ;
    }

}

$test = new Person;
$test->firstName = "Hendro";
$test->secret = "My Secret";
$test->nickName = "Rambo";

echo "<pre>".print_r($test, true)."</pre>";
unset($test->nickName);
echo "<pre>".print_r($test, true)."</pre>";
unset($test->secret);
echo "<pre>".print_r($test, true)."</pre>";