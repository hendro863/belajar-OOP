<?php
/**
*
*/
class Animal
{
    //harus berupa string,integer,float.boolean
    const EYES = 2;
}

class Pet extends Animal
{
    const EYES = 'two';
}

echo Animal::EYES."<br>";
echo Pet::EYES;