<?php
/**
*
*/
class Person
{

    function say_hello()
    {
        echo "Hello World".'<br>' ;
    }

    function full_name($first_name,$last_name = NULL)
    {
        return $first_name.' '.$last_name;
    }

    function global_var($say)
    {
        global $global;
        echo $global.' say '.$say.'<br />';
    }

    function &test_References(&$args)
    {
        $args = 'bar';
        $return_value = 'foo and'.$args.'<br>';
        return $return_value;
    }

    function static_var()
    {
        static $num = 1;
        echo $num.'<br>';
        $num++;
    }
}

$orang = new Person;
echo method_exists($orang, 'say_hello')? "True" : "False" ;
echo '<pre>'.print_r(get_class_methods($orang),true).'</pre>' ;
$orang->say_hello();
echo call_user_func(array($orang, 'full_name'),'Hendro');
$global = 'Ghost';
echo call_user_func_array(array($orang, 'global_var'), array('boo!'));
$bar = 'foo';
$var_ref = & $orang->test_References($bar);
echo $var_ref;
echo $bar."<br>";
$orang->static_var();
$orang->static_var();
$orang->static_var();