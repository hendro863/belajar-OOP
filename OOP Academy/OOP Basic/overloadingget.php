<?php
/**
*
*/
class Person
{
    public  $firstName;
    private $secret;
    private $request = array();

    public function __set($name, $value)
    {
        $this->request[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->request)) {
            return $this->request[$name];
        }
    }
}

$test = new Person;
$test->firstName = "Hendro";
$test->nickName = "Senda";
$test->secret = "My Secret";
echo "<pre>".print_r($test,true).'</pre>';
echo $test->secret."<br>";
echo $test->nickName."<br>";
$test->lastName = "Jog";
echo "<pre>".print_r($test,true).'</pre>';