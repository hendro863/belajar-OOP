<?php
class Animal
{
    public $noise = "Meong";
}

class Pet extends Animal
{
    public function make_noise()
    {
        return $this->noise;
    }
}

class Person
{
    function pet_noise($pet)
    {
        return $pet->noise;
    }
}

$cat   = new Animal;
$kitty = new Pet;
$john  = new Person;

// echo '<b>'.$cat->noise.'<b>'.'<br>';
echo '<i>'.$kitty->make_noise().'<i><br>' ;
echo '<u>'.$john->pet_noise(new pet).'<u><br>';
