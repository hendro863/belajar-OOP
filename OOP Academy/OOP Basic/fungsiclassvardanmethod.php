<?php
/**
*
*/
class Person
{
    public static $year;
    public $name;
    protected $problem;
    private $secret;

    protected function working()
    {

    }

    private function sleeping()
    {

    }

    public function getVars()
    {
        echo '<pre> Vars Person : '.print_r(get_class_vars(get_class()), true).'</pre>';
    }

    public function getMethods()
    {
        echo '<pre> Method Person : '.print_r(get_class_vars(get_class()),true).'</pre>';
    }
}

class Friend extends Person
{
    public function getVars()
    {
        echo '<pre> Vars Person : '.print_r(get_class_vars(get_class()), true).'</pre>';
    }

    public function getMethods()
    {
        echo '<pre> Method Person : '.print_r(get_class_vars(get_class()),true).'</pre>';
    }
}
'<pre>Vars Person (Public) : '.print_r(get_class_vars('Person')).'</pre>';
'<pre>Vars Friend (Public) : '.print_r(get_class_vars('Friend')).'</pre>';

'<pre>Method Person (public) :'.print_r(get_class_methods('Person')).'</pre>';
'<pre>Method Friend (public) :'.print_r(get_class_methods('Friend')).'</pre>';

Person::getVars();
Friend::getVars();
Person::getMethods();
Friend::getMethods();