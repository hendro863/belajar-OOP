<?php
/**
*
*/
/*
    -static digunakan variable di dalam function atau method suatu class
    -static digunakan property di dalam class
    -static digunakan method di dalam class
    -static digunakan class dalam 'late state Bindings'
*/

class Person
{
    public $firstName;
    public $lastName;

    public function fullName()
    {
        if (isset($this)) {
            return $this->firstName.' '.$this->lastName;
        }else {
            return 'Anonymous';
        }
    }
}
$test = new Person;
$test->firstName = "Hendro ";
$test->lastName  = "Rahmat";
$class = 'Person';
echo Person::fullName()."<br>";
// echo Person::$firstName;
//variabel firstname yg dikomen diatas tidak bisa di akses dikarenakan tipe variabel bukan static namun public
echo $class::fullName()."<br>";
echo $test->fullName();