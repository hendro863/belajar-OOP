<?php
/**
*
*/
class Person
{
    static $year =2009;
    public function nextyear()
    {
        Person::$year++;
    }

    public function currentYear()
    {
        echo Person::$year."<br>";
    }
}

Person::currentYear();//2009
Person::nextyear();
Person::currentYear();//2010

$test = new Person;

$test->currentYear(); //2010
$test->nextyear();
$test->currentYear(); //2011

