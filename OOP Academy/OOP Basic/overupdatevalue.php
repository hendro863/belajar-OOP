<?php
/**
*
*/
class Person
{
    public $firstName;
    private $secret;
    private $request = array();

    public function __set($name, $value)
    {
        echo "---<b>{$name}</b> Setting to <i>".var_export($value, true).'</i><br>';
        return $this->request[$name] = $value;
    }

    public function __get($name)
    {
        echo "--Getting <u> {$name} </u><br />";
        if (array_key_exists($name, $this->request)) {
            return $this->request[$name];
        }
    }

    public function __isset($name)
    {
        echo "is {$name} already set ? ";
        return isset($this->request[$name])."</br>";
    }

    public function __unset($name)
    {
        echo '<pre>'.print_r($this,true).'</pre>';
        unset($this->request[$name]) ;
    }

}

$test = new Person();
$test->firstName = "Hendro";
echo '<pre>'.print_r($test,true).'</pre>';
?>
<?php
echo '<ins> Integer or float operators </ins><br />';
$test->age = 20;
echo '<hr />';
$test->age +=1;
echo '<hr>';
echo isset($test->age)? "Yes" : "No","<br>";
unset($test->age);
?>

<?php
echo '<ins>String operators</ins><br />';
$test->hobby = 'Sleep';
$test->hobby .= 'All Day';
echo isset($test->hobby) ? 'Yes':'No','<br>';
unset($test->hobby);
?>

<?php
echo "<ins>Array operators</ins><br />";
$test->friends = array('Coba');
$test->friends += array('Senda', 'Saputra','Rahmat');
echo isset($test->friends) ? "Yes" : "No";
unset($test->friends);
?>