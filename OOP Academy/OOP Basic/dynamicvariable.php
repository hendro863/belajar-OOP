<?php
   /**
   *
   */
class Person
{
    public static $year = 2000;
    public $firstName, $lastName, $data = array('gender'=> 'male');

    public function __construct($fName = 'Anonymous', $lName = 'Anonymous')
    {
        $this->firstName = $fName;
        $this->lastName  = $lName;
    }

    public function fullName()
    {
        return isset($this)? $this->firstName.' '.$this->lastName : 'Anonymous';
    }
}

$class = 'Person';
$getYear = 'year';
$getFirstName = 'firstName';
$getFullName = 'fullName';
$getData = 'data';
$name = array(
        'first' => 'firstName',
        'last' => 'lastName',
        'full' => 'fullName'
    );

echo Person::$year."<br>";
echo $class::$year."<br>";
echo $class::$$getYear."<br>";
echo $class::fullName()."<br>";

$test = new $class('Hendro','Rahmat');
echo $test->firstName."<br>"; //hendro
echo $test->$getFirstName."<br>";//hendro
echo $test->{'lastName'}."<br>";//rahmat
echo $test->$name['last']."<br>";//rahmat
echo $test->fullName()."<br>";// Hendro
echo $test->$getFullName()."<br>";
echo $test->{'fullName'}()."<br>";
echo $test->$name['full']()."<br>";
echo $test->data['gender']."<br>";
echo $test->{$getData}['gender']."<br>";//male
