<?php
/**
*
*/
class Person
{
    public $firstName;
    public $lastName;

    public function __construct($fName, $lName)
    {
        $this->firstName = $fName;
        $this->lastName = $lName;

        echo "Hy ".$this->fullName();
    }

    public function fullName()
    {
        return $this->firstName.' '.$this->lastName;
    }
}

$test = new Person("Hendro","Rahmat");
$john = new Person("Senda","Hendy");