<?php
/**
*
*/
class Animal
{

    public function test()
    {
        return 'Ini Animal';
    }

}

class Bird extends Animal
{
    // public function test()
    // {
    //     return 'ini punya Bird';
    // }
}

class Eagle extends Bird
{
    public $data = 'Hello World';

    public function suara()
    {
        return 'ini fungsi';
    }

    // public function test()
    // {
    //     return $this->suara();
    // }
}

$eagle = new Eagle();
echo $eagle->test();
$data = [
    'data'=>[
        'name'=>'cristo',
        'umur'=>13,
        'no_hp'=>[
            '123',
            '321',
        ],
    ]
];

print_r($data);

// $cat = new Animal;
// $duck = new Bird;

// echo var_export(get_parent_class('Animal'),true)."<br>";//false
// echo var_export(get_parent_class('Bird'),true)."<br>";//Animal
// echo var_export(get_parent_class($cat),true)."<br>";
// echo var_export(get_parent_class($duck),true)."<br>";
// echo var_export(get_parent_class('Eagle'),true)."<br>";//Bird

// echo (is_subclass_of('Bird', 'Animal') ? "True" : "false")."<br>";
// //Apakah class Bird adalah class child/turunan dari class animal ? iyah : tidak
// echo (is_subclass_of('Animal', 'Bird')? 'True' : 'false')."<br>";

// echo ($duck instanceof Animal ? 'True' : 'False');