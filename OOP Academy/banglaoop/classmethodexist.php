<?php
/**
*
*/
class Person
{
    public function describe()
    {
        echo "Method exist";
    }
}

if (class_exists('Person')) {
    echo "Class exist <br>";
    $p = new Person();

    if (method_exists($p,'describe')) {
        $p->describe();
    } else{
        echo "Method not found";
    }
}else {
    echo "Class not found";
}
