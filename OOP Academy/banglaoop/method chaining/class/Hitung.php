<?php
/**
*
*/
class Hitung
{
    public $a=0;
    public $b=0;
    public $result;

    public function getValue($x, $z)
    {
        $this->a = $x;
        $this->b = $z;
        return $this;
    }

    public function getResult()
    {
        $this->result = $this->a * $this->b;
        return $this->result;
    }
}