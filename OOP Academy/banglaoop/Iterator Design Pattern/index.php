<?php

spl_autoload_register(function($className){
    include 'class/'.$className.'.php';
});

$blogPost = getAllPost();
$posts = new Post();
foreach ($posts as $post) {
    echo $post->getTitle();
    echo $post->getContent();
    echo $post->getDate();
    echo $post->getAuthor();

    $comments = new comments($post->getComments());
    foreach ($comments as $comment ) {
        echo $comment->getCommentAuthor();
        echo $comment->getCommentContent();
    }
}