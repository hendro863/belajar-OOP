<?php
/**
*
*/
class Person
{
    public $name;
    public $age;

    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age  = $age;
    }

    public function personDetails()
    {
        echo "Hay {$this->name} yang berumur {$this->age}";
    }
}

$person = new Person("Hendro", 17);
$person->personDetails();