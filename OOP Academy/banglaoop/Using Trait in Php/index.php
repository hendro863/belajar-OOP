<?php
/**
*
*/
trait Java
{
    public function javaCoder()
    {
        return "I Lover Java <br>";
    }
}

trait Php
{
    public function phpCoder()
    {
        return "I love PHP <br>";
    }
}

trait JavaPHP
{
    use Java, Php;
}

class CoderOne
{
    use JavaPHP;
}

class CoderTwo
{
    //jika ada nama fungsi yg sama maka fungsi yg dijalankan yg ada di dalam class

    use java;
    public function javaCoder()
    {
        return "I Love Java and php ";
    }
}

$c1 = new CoderOne();
echo $c1->javaCoder();
echo $c1->phpCoder();

$c2 = new CoderTwo();
echo $c2->javaCoder();
