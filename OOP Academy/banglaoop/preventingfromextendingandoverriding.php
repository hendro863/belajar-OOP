<?php
/**
*
*/
class Person
{
    public $name;
    public $age;

    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age  = $age;
    }

    public function display()
    {
        echo "Hay {$this->name} yang berumur {$this->age}";
    }
}

class Admin extends Person
{
    public $level="Super Admin";

    public function display()
    {
        echo "Username is {$this->name} and User id is {$this->age}
                and user Level {$this->level}";
    }
}

// $person = new Person("Hendro", 17);
// $person->personDetails();
$jos = new Admin("Senda", 18);
$jos->display();