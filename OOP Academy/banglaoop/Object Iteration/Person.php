<?php
/**
*
*/
class Person
{
    public $name = "Hendro";
    public $age  = 17;
    public $skill= "Blogging";

    private   $email    = "hendro863@gmail.com";
    protected $password = "12345";

    public function iteratorInner(){
        echo "Inside Class <br />";
        foreach ($this as $key => $value) {
            echo "<pre>";
            echo "$key=>$value";
            echo "</pre>";
        }
    }
}

$person = new Person();
$person->iteratorInner();
