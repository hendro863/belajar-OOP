<?php

// include 'class/Java.php';
// include 'class/Php.php';
// include 'class/Ruby.php';

spl_autoload_register(function($className){
    include 'class/'.$className.'.php';
});

$java = new java();
$php  = new Php();
$ruby = new Ruby();