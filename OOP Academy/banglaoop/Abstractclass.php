<?php
/**
*Abstract class adalah class yang mempunyai y g p y setidaknya satu abstract
method. Abstract method adalah method yang tidak memiliki body (hanya deklarasi
method) memiliki body (hanya deklarasi method). Abstract class tidak bisa dibuat
obyeknya. Obyek hanya bisa dibuat dari non-abstract class (concrete class).
*/
abstract class Student
{
    public $name;
    public $age;

    public function details()
    {
        echo $this->name." is ".$this->age." years Old <br>";
    }

    abstract public function School();
}

class Boy extends Student
{
    public function describe()
    {
        return $this->details()."And i am high school student <br>";
    }

    public function School()
    {
        return "I Like to read story book";
    }
}

$s = new Boy();
$s->name ="Hendro";
$s->age = "15";

echo $s->describe();
echo $s->School();