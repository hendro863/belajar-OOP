<?php

spl_autoload_register(function($className){
    include 'class/'.$className.'.php';
});

$user = new User();
$msg = $user->getMsg();

switch ($msg) {
    case 'email':
        $objctmsg = new SendEmail();
        break;
    case 'sms':
        $objctmsg = new SendSms();
        break;
    case 'fax':
        $objctmsg = new SendFax();
        break;
}

$objctmsg->notification();