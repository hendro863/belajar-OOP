<?php

spl_autoload_register(function($className){
    include 'class/'.$className.".php";
});

$java = new Language();
$java->setCat("OOP");
$java->setFramework("Spring");

$php = clone $java;

$php->setFramework("Laravel");
$php->setCat("e");

echo $java->getCat()."<br />";
echo $java->getFramework()."<br />";

echo $php->getCat()."<br />";
echo $php->getFramework()."<br />";