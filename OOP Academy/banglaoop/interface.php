<?php
/**
*
*/
interface School
{
    public function mySchool();
}

interface College
{
    public function myCollege();
}
interface Varsity
{
    public function myVarsity();
}

class Teacher implements School, College, Varsity
{

    public function __construct()
    {
        $this->mySchool();
        $this->myCollege();
        $this->myVarsity();
    }

    public function mySchool()
    {
        echo "I am a School teacher";
    }

    public function myCollege()
    {
        echo "Universitas 17 Agustus Surabaya";
    }

    public function myVarsity()
    {
        echo "I am a Varsity Teacher";
    }
}

class Student implements School, College, Varsity
{

    public function __construct()
    {
        $this->mySchool();
        $this->myCollege();
        $this->myVarsity();
    }

    public function mySchool()
    {
        echo "I am a School Student"."<br>";
    }

    public function myCollege()
    {
        echo "Universitas 17 Agustus Surabaya Student"."<br>";
    }

    public function myVarsity()
    {
        echo "I am a Varsity Student"."<br>";
    }
}

$teacher = new Teacher();
$student =  new Student();