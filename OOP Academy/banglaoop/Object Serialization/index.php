<?php
/**
*
*/
class Progamming
{
    public    $html;
    public    $css;
    private   $php;
    protected $java;

    function __construct()
    {
        $this->html = "I know HTML";
        $this->css  = "I Love Css";
        $this->php  = "I am PHP Coder";
        $this->java = "I am Java programmer";
    }
}

$pro = new Progamming();
$ser = serialize($pro);
//serialize () =  untuk mengubah variable kompleks menjadi sebuah teks
// file_put_contents("progamming.txt", $ser);
// echo $ser;
$getcont = file_get_contents("progamming.txt");
$unser = unserialize($getcont);
//unserialize() akan mengembalikan teks tersebut ke dalam sebuah array atau object
echo "<pre>";
print_r($unser);
echo "</pre>";