<?php
/**
*
*/
class Person
{
    public $name;
    public $age;
    public $id;

    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age  = $age;
    }

    public function personDetails()
    {
        echo "Hay {$this->name} yang berumur {$this->age}";
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function __destruct()
    {
        if (!empty($this->id)) {
            echo "Saving Person";
        }
    }
}

$person = new Person("Hendro", 17);
$person->setId(22);
unset($person);