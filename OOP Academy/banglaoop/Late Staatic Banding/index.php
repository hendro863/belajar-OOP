<?php
/**
*
*/
spl_autoload_register(function($className){
    include "class/".$className.".php";
});

class phpChild extends Php
{
    public static function getClass()
    {
        return __CLASS__;
    }
}

class Coba extends phpChild
{
    public static function getClass()
    {
        return __CLASS__;
    }
}

$php = new Php();
$php->framework();

$childphp = new phpChild();
$childphp->framework();

$coba = new Coba();
$coba->framework();