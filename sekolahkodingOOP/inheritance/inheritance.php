<?php 
/**
* 
*/
class robot
{
	var $nama;

	function __construct($nama)
	{
		$this->nama = $nama;
	}

	function set_nama($namaBaru)
	{
		$this->nama = $namaBaru;
	}

	function get_nama()
	{
		return $this->nama;
	}
}

/**
* 
*/
class hewan extends robot
{
	
	function __construct($nama)
	{
		$this->nama = $nama;
	}
}