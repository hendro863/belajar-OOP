<?php 
/**
* 
*/
class Robot
{
	//public, protected,private
	protected $nama;

    function __construct($nama)
	{
		$this->nama = $nama;
	}

	function get_nama()
	{
		return $this->nama;
	}
}

class Hewan extends Robot
{
	function __construct($namaBaru)
	{
		$this->nama = $namaBaru;
	}
}