<?php

$FirstName = "Hendro";
$LastName = "Rahmat";

$namalengkap = function() use ($FirstName, $LastName)
{
    return $FirstName." ".$LastName;
};

function sapa($nama)
{
    echo "Hy ".$nama."<br />";
}

sapa($namalengkap());

/* --------------Contoh Closure Lebih jelas ada di bawah --------------*/

 $minimal = 60;
 $mahasiswa = array(
    array("nama" => "Nurul Imam",
          "nilai" => 90),
    array("nama" => "Zaenal Muttaqien",
          "nilai" => 55)
    );

  echo '<pre>'.print_r($mahasiswa,true).'</pre>';

  array_walk($mahasiswa, function ($jos) use ($minimal) {
    echo "Nama : ".$jos['nama']."<br />";
    echo "Nilai : ".$jos['nilai']."<br />";
    echo "Keterangan : ";

    if ($jos['nilai'] >= $minimal) {
      echo "Lulus <br /><br />";
    } else {
      echo "Gagal <br /><br />";
    }
  });